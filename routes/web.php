<?php

use App\Http\Controllers\Mahasiswa;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/daftar-mahasiswa', [Mahasiswa::class, 'listAll']);

Route::get('/tambah-mahasiswa', [Mahasiswa::class, 'showAddForm']);
Route::post('/tambah-mahasiswa', [Mahasiswa::class, 'add']);
