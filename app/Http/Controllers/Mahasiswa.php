<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa as ModelsMahasiswa;
use Illuminate\Http\Request;

class Mahasiswa extends Controller
{
    public function listAll()
    {
        $daftarMahasiswa = ModelsMahasiswa::all();
        return view('daftar-mahasiswa')->with('daftarMahasiswa', $daftarMahasiswa);
    }

    public function showAddForm()
    {
        return view('tambah-mahasiswa');
    }

    public function add(Request $request)
    {
        ModelsMahasiswa::create([
            'nim' => $request->nim,
            'nama' => $request->nama
        ]);

        return redirect('/daftar-mahasiswa')->with('success', 'Data berhasil ditambahkan');
    }
}
