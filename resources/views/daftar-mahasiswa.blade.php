<!DOCTYPE html>
<html lang="id">

<head>
    <title>Daftar Seluruh Mahasiswa</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <h2>Daftar Semua Mahasiswa</h2>
    <table border="1" cellspacing="0" cellpadding="4">
        <thead style="font-weight: bold; background-color: lightgray;">
            <tr>
                <td>id</td>
                <td>nim</td>
                <td>nama</td>
                <td>created_at</td>
                <td>updated_at</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($daftarMahasiswa as $mahasiswa)
                <tr>
                    <td>{{ $mahasiswa->id }}</td>
                    <td>{{ $mahasiswa->nim }}</td>
                    <td>{{ $mahasiswa->nama }}</td>
                    <td>{{ $mahasiswa->created_at }}</td>
                    <td>{{ $mahasiswa->updated_at }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <a href="/tambah-mahasiswa">+ Tambah Mahasiswa</a>
</body>

</html>
