<!DOCTYPE html>
<html lang="id">

<head>
    <title></title>
    <meta charset="UTF-8">
</head>

<body>

    <h2>Tambah Mahasiswa</h2>
    <form action="/tambah-mahasiswa" method="post" style="display: flex; flex-direction: column; gap: 4px;">
        @csrf
        <div>
            <label for="nim">NIM:</label>
            <input type="text" name="nim" id="nim">
        </div>
        <div>
            <label for="nama">Nama:</label>
            <input type="text" name="nama" id="nama">
        </div>
        <input type="submit" value="Simpan dan tambah" style="width: fit-content;">
    </form>
</body>

</html>
